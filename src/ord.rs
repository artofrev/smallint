use crate::smallint::{SmallIntType, SmallUintType};
use crate::SmallInt;
use crate::SmallUint;
use std::cmp::Ordering;

impl PartialEq for SmallUint {
    fn eq(&self, other: &SmallUint) -> bool {
        match (&self.0, &other.0) {
            (SmallUintType::Inline(i), SmallUintType::Inline(j)) => i == j,
            (SmallUintType::Heap((p, s)), SmallUintType::Heap((q, t))) => {
                let slice1 = unsafe { core::slice::from_raw_parts(*p, *s) };
                let slice2 = unsafe { core::slice::from_raw_parts(*q, *t) };
                slice1 == slice2
            }
            (_, _) => false,
        }
    }
}

impl Eq for SmallUint {}

impl PartialOrd for SmallUint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SmallUint {
    fn cmp(&self, other: &Self) -> Ordering {
        match (&self.0, &other.0) {
            (SmallUintType::Inline(i), SmallUintType::Inline(j)) => i.cmp(j),
            (SmallUintType::Inline(_), SmallUintType::Heap((_, _))) => Ordering::Less,
            (SmallUintType::Heap((_, _)), SmallUintType::Inline(_)) => Ordering::Greater,
            (SmallUintType::Heap((p, s)), SmallUintType::Heap((q, t))) => match s.cmp(t) {
                Ordering::Equal => {
                    let slice1 = unsafe { core::slice::from_raw_parts(*p, *s) };
                    let slice2 = unsafe { core::slice::from_raw_parts(*q, *t) };
                    for i in 0..*s {
                        match slice1[s - 1 - i].cmp(&slice2[s - 1 - i]) {
                            Ordering::Equal => {}
                            o => return o,
                        }
                    }
                    Ordering::Equal
                }
                o => o,
            }
        }
    }
}

impl PartialEq for SmallInt {
    fn eq(&self, other: &SmallInt) -> bool {
        match (&self.0, &other.0) {
            (SmallIntType::Inline(i), SmallIntType::Inline(j)) => i == j,
            (SmallIntType::Heap((p, s)), SmallIntType::Heap((q, t))) => {
                if s != t {  // need to compare signs, at minimum
                    return false;
                }
                let slice1 = unsafe { core::slice::from_raw_parts(*p, s.unsigned_abs()) };
                let slice2 = unsafe { core::slice::from_raw_parts(*q, t.unsigned_abs()) };
                slice1 == slice2
            }
            (_, _) => false,
        }
    }
}

impl Eq for SmallInt {}

impl PartialOrd for SmallInt {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SmallInt {
    fn cmp(&self, other: &Self) -> Ordering {
        let a_sign = match &self.0 {
            SmallIntType::Inline(i) => i.cmp(&0),
            SmallIntType::Heap((_, s)) => s.cmp(&0),
        };
        let b_sign = match &other.0 {
            SmallIntType::Inline(j) => j.cmp(&0),
            SmallIntType::Heap((_, t)) => t.cmp(&0),
        };
        match a_sign.cmp(&b_sign) {
            Ordering::Equal => match a_sign {
                Ordering::Less => other.unsigned_abs().cmp(&self.unsigned_abs()),
                Ordering::Equal => Ordering::Equal,
                Ordering::Greater => self.unsigned_abs().cmp(&other.unsigned_abs()),
            },
            o => o,
        }
    }
}
