use crate::SmallInt;
use crate::SmallUint;

use crate::smallint::{SmallIntType, SmallUintType};

#[cfg(feature = "num-bigint")]
use num_bigint::{BigInt, BigUint};

impl core::fmt::Debug for SmallInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            SmallIntType::Inline(i) => {
                write!(f, "{}", i)?;
            }
            SmallIntType::Heap((_r, _s)) => {
                #[cfg(feature = "num-bigint")]
                write!(f, "{}", BigInt::from(self))?;

                #[cfg(not(feature = "num-bigint"))]
                todo!();
            }
        }
        Ok(())
    }
}

impl core::fmt::Debug for SmallUint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            SmallUintType::Inline(i) => {
                write!(f, "{}", i)?;
            }
            SmallUintType::Heap((_r, _s)) => {
                #[cfg(feature = "num-bigint")]
                write!(f, "{}", BigUint::from(self))?;

                #[cfg(not(feature = "num-bigint"))]
                todo!();
            }
        }
        Ok(())
    }
}

impl core::fmt::Display for SmallInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl core::fmt::Display for SmallUint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl core::fmt::UpperHex for SmallUint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            SmallUintType::Inline(i) => write!(f, "0x{:X}", i),
            SmallUintType::Heap((r, s)) => {
                let slice = unsafe { core::slice::from_raw_parts(r, s) };
                let mut iter = slice.iter().rev();
                if let Some(i) = iter.next() {
                    write!(f, "0x{:X}", i)?;
                }
                for i in iter {
                    write!(f, "{:08X}", i)?;
                }
                Ok(())
            }
        }
    }
}

impl core::fmt::LowerHex for SmallUint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            SmallUintType::Inline(i) => write!(f, "0x{:x}", i),
            SmallUintType::Heap((r, s)) => {
                let slice = unsafe { core::slice::from_raw_parts(r, s) };
                let mut iter = slice.iter().rev();
                if let Some(i) = iter.next() {
                    write!(f, "0x{:x}", i)?;
                }
                for i in iter {
                    write!(f, "{:08x}", i)?;
                }
                Ok(())
            }
        }
    }
}

impl core::fmt::UpperHex for SmallInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            SmallIntType::Inline(i) => match i {
                x if x >= 0 => write!(f, "0x{:X}", i),
                x if x < 0 => write!(f, "-0x{:X}", -i),
                _ => panic!("This should not happen."),
            },
            SmallIntType::Heap((r, s)) => {
                let sign = match s.signum() {
                    x if x >= 0 => "",
                    x if x < 0 => "-",
                    _ => panic!("This should not happen."),
                };

                let slice = unsafe { core::slice::from_raw_parts(r, s.unsigned_abs()) };
                let mut iter = slice.iter().rev();
                if let Some(i) = iter.next() {
                    write!(f, "{}0x{:X}", sign, i)?;
                }
                for i in iter {
                    write!(f, "{:08X}", i)?;
                }
                Ok(())
            }
        }
    }
}

impl core::fmt::LowerHex for SmallInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            SmallIntType::Inline(i) => match i {
                x if x >= 0 => write!(f, "0x{:x}", i),
                x if x < 0 => write!(f, "-0x{:x}", -i),
                _ => panic!("This should not happen."),
            },
            SmallIntType::Heap((r, s)) => {
                let sign = match s.signum() {
                    x if x >= 0 => "",
                    x if x < 0 => "-",
                    _ => panic!("This should not happen."),
                };

                let slice = unsafe { core::slice::from_raw_parts(r, s.unsigned_abs()) };
                let mut iter = slice.iter().rev();
                if let Some(i) = iter.next() {
                    write!(f, "{}0x{:x}", sign, i)?;
                }
                for i in iter {
                    write!(f, "{:08x}", i)?;
                }
                Ok(())
            }
        }
    }
}
