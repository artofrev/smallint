#![deny(missing_docs)]
#![warn(clippy::all)]

//! A crate for small integer optimization. Provides the [`SmallInt`] type. When possible this will
//! inline an integer and store it on the stack if that integer is small. However, for larger values,
//! this will be instead stored on the heap as a pointer to a `u32` slice, a length, and a sign.

// Invariant: If a small integer is within the bounds of an inline value, it must be inline.
// Invariant: If a small integer is on the heap, the size is the minimum digits required to
// represent it.

mod smallint;

pub use crate::smallint::SmallInt;
pub use crate::smallint::SmallUint;

mod error;

pub use error::SmallIntError;

mod convert;
mod ord;

mod logic;
mod ops;

mod bigint;

mod pretty;

mod tests;
