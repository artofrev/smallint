/// An error that occurred when processing a `SmallInt`.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SmallIntError {
    /// Conversion error when converting from SmallInt to other integer types.
    ConversionError,
}

impl std::error::Error for SmallIntError {}

impl core::fmt::Display for SmallIntError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let Self::ConversionError = self;
        write!(f, "Error converting integer to SmallInt.")
    }
}
