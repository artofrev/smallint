use crate::ops::{add_two_slices, sub_two_slices};
use crate::smallint::{SmallIntType, SmallUintType};
use crate::{SmallInt, SmallUint};
use core::cmp::Ordering;
use core::mem::ManuallyDrop;
use core::ops::{BitAnd, BitOr, BitXor};
use core::ops::{BitAndAssign, BitOrAssign, BitXorAssign};

macro_rules! logic_op {
    (
        $imp:ident, $imp_assign:ident, $typ:ident, $typ_inner:ident, $fun:ident, $fun_assign:ident;
        $i:ident, $j:ident, $p:ident, $q:ident, $s:ident, $t:ident;
        $inline_heap:tt, $heap_heap:tt
    ) => {
        impl<'a, 'b> $imp<&'a $typ> for &'b $typ {
            type Output = $typ;

            fn $fun(self, rhs: &$typ) -> Self::Output {
                match (&self.0, &rhs.0) {
                    (&$typ_inner::Inline($i), &$typ_inner::Inline($j)) => {
                        $typ($typ_inner::Inline($i.$fun($j)))
                    }

                    (&$typ_inner::Inline($i), &$typ_inner::Heap(($p, $s)))
                    | (&$typ_inner::Heap(($p, $s)), &$typ_inner::Inline($i)) => {
                        $inline_heap
                    }

                    (&$typ_inner::Heap(($p, $s)), &$typ_inner::Heap(($q, $t))) => {
                        $heap_heap
                    }
                }
            }
        }

        impl<'a> $imp<$typ> for &'a $typ {
            type Output = $typ;

            fn $fun(self, rhs: $typ) -> Self::Output {
                self.$fun(&rhs)
            }
        }

        impl<'a> $imp<&'a $typ> for $typ {
            type Output = $typ;

            fn $fun(self, rhs: &$typ) -> Self::Output {
                (&self).$fun(rhs)
            }
        }

        impl $imp<$typ> for $typ {
            type Output = $typ;

            fn $fun(self, rhs: $typ) -> Self::Output {
                (&self).$fun(&rhs)
            }
        }

        impl<'a> $imp_assign<&'a $typ> for $typ {
            fn $fun_assign(&mut self, rhs: &'a $typ) {
                *self = (&*self).$fun(rhs);
            }
        }

        impl $imp_assign<$typ> for $typ {
            fn $fun_assign(&mut self, rhs: $typ) {
                *self = (&*self).$fun(rhs);
            }
        }
    };
    (
        $imp:ident, $imp_assign:ident, $typ:ident, $typ_inner:ident, $fun:ident, $fun_assign:ident;
        $i:ident, $j:ident, $p:ident, $q:ident, $s:ident, $t:ident, $slice:ident, $slice1:ident, $slice2:ident, $min:ident, $res:ident;
        $inline_heap_inner:tt, $heap_heap_create_res:tt, $heap_heap_return:tt
    ) => {
        logic_op! {
            $imp, $imp_assign, $typ, $typ_inner, $fun, $fun_assign;
            $i, $j, $p, $q, $s, $t;
            {
                let $slice = unsafe { core::slice::from_raw_parts($p, $s) };

                $inline_heap_inner
            },
            {
                let $slice1 = unsafe { core::slice::from_raw_parts($p, $s) };
                let $slice2 = unsafe { core::slice::from_raw_parts($q, $t) };

                let $min = std::cmp::min($slice1.len(), $slice2.len());

                #[allow(unused_mut)]
                let mut $res = $heap_heap_create_res;

                $heap_heap_return
            }
        }
    };
}


macro_rules! heap_to_inline {
    ($typ_inline:ident; $slice:ident) => {
        let mut j = 0;
        for i in 0..4 {
            j <<= 32;
            j |= $slice[3 - i] as $typ_inline;
        }
        j
    }
}

macro_rules! inline_heap_to_inline {
    ($fun:ident, $typ:ident, $typ_inner:ident, $typ_inline:ident; $i:ident, $slice:ident) => {
        let j = { heap_to_inline! { $typ_inline; $slice } };
        $typ($typ_inner::Inline($i.$fun(j)))
    }
}

macro_rules! inline_heap_to_heap {
    ($fun_assign:ident, $typ:ident, $typ_inner:ident; $i:ident, $slice:ident) => {
        let mut retvec = $slice.to_vec();

        let mut v = $i;
        #[allow(clippy::needless_range_loop)]
        for r in 0..4 {
            retvec[r].$fun_assign(v as u32);

            v >>= 32;
        }

        let mut retslice = ManuallyDrop::new(retvec.into_boxed_slice());

        $typ($typ_inner::Heap((retslice.as_mut_ptr(), retslice.len().try_into().unwrap())))
    }
}


macro_rules! heap_heap_create_res_shortest {
    ($fun:ident; $slice1:ident, $slice2:ident, $min:ident) => {
        let mut res = Vec::with_capacity($min);
        for l in 0..$min {
            res.push($slice1[l].$fun($slice2[l]));
        }
        res
    }
}

macro_rules! heap_heap_create_res_longest {
    ($fun:ident; $slice1:ident, $slice2:ident, $min:ident) => {
        let mut res = if $slice1.len() > $slice2.len() {
            $slice1.to_vec()
        } else {
            $slice2.to_vec()
        };
        for l in 0..$min {
            res[l] = $slice1[l].$fun($slice2[l]);
        }
        res
    }
}


macro_rules! return_heap_inner {
    ($typ:ident, $typ_inner:ident; $res:ident) => {
        let mut slice = ManuallyDrop::new($res);
        $typ($typ_inner::Heap((slice.as_mut_ptr(), slice.len().try_into().unwrap())))
    }
}

macro_rules! return_heap_inner_neg {
    ($typ:ident, $typ_inner:ident; $res:ident) => {
        let mut slice = ManuallyDrop::new($res);
        $typ($typ_inner::Heap((slice.as_mut_ptr(), -isize::try_from(slice.len()).unwrap())))
    }
}

macro_rules! return_heap {
    ($typ:ident, $typ_inner:ident; $res:ident) => {
        let res = $res.into_boxed_slice();
        return_heap_inner! { $typ, $typ_inner; res }
    }
}

macro_rules! return_heap_neg {
    ($typ:ident, $typ_inner:ident; $res:ident) => {
        let res = $res.into_boxed_slice();
        return_heap_inner_neg! { $typ, $typ_inner; res }
    }
}

macro_rules! heap_heap_return_any {
    ($typ:ident, $typ_inner:ident, $typ_inline:ident; $res:ident) => {
        while $res.len() != 1 && $res[$res.len() - 1] == 0 {
            $res.pop();
        }

        if $res.len() <= 4 {
            let mut r = 0;
            for t in 0..$res.len() {
                r <<= 32;
                r |= $res[$res.len() - 1 - t] as $typ_inline;
            }
            $typ($typ_inner::Inline(r))
        } else {
            let mut slice = ManuallyDrop::new($res.into_boxed_slice());
            $typ($typ_inner::Heap((slice.as_mut_ptr(), slice.len().try_into().unwrap())))
        }
    }
}

logic_op! {
    BitAnd, BitAndAssign, SmallUint, SmallUintType, bitand, bitand_assign;
    i, j, p, q, s, t, slice, slice1, slice2, min, res;
    { inline_heap_to_inline! { bitand, SmallUint, SmallUintType, u128; i, slice } },
    { heap_heap_create_res_shortest! { bitand; slice1, slice2, min } },
    { heap_heap_return_any! { SmallUint, SmallUintType, u128; res } }
}

logic_op! {
    BitOr, BitOrAssign, SmallUint, SmallUintType, bitor, bitor_assign;
    i, j, p, q, s, t, slice, slice1, slice2, min, res;
    { inline_heap_to_heap! { bitor_assign, SmallUint, SmallUintType; i, slice } },
    { heap_heap_create_res_longest! { bitor; slice1, slice2, min }},
    { return_heap! { SmallUint, SmallUintType; res } }
}

logic_op! {
    BitXor, BitXorAssign, SmallUint, SmallUintType, bitxor, bitxor_assign;
    i, j, p, q, s, t, slice, slice1, slice2, min, res;
    { inline_heap_to_heap! { bitxor_assign, SmallUint, SmallUintType; i, slice } },
    { heap_heap_create_res_longest! { bitxor; slice1, slice2, min }},
    { heap_heap_return_any! { SmallUint, SmallUintType, u128; res } }
}



fn bitor_two_slices(slice1: &[u32], slice2: &[u32]) -> Vec<u32> {
    let mut result = if slice1.len() > slice2.len() {
        slice1.to_vec()
    } else {
        slice2.to_vec()
    };
    for l in 0..std::cmp::min(slice1.len(), slice2.len()) {
        result[l] = slice1[l] | slice2[l];
    }
    result
}

fn bitand_two_slices_mixed_sign(positive: &[u32], negative: &[u32]) -> Vec<u32> {
    // a & (-b) = (a | (b - 1)) + 1 - b
    // 0 is not negative, so we can ignore it
    let sub1 = sub_two_slices(negative, &[1]);
    let or = bitor_two_slices(positive, &sub1);
    let add = add_two_slices(&or, &[1]);
    let sub2 = sub_two_slices(&add, negative);
    sub2
}

fn bitand_two_slices(slice1: &[u32], slice2: &[u32]) -> Vec<u32> {
    let mut result = if slice1.len() > slice2.len() {
        slice1.to_vec()
    } else {
        slice2.to_vec()
    };
    for l in 0..std::cmp::min(slice1.len(), slice2.len()) {
        result[l] = slice1[l] | slice2[l];
    }
    result
}

fn bitor_two_slices_mixed_sign(positive: &[u32], negative: &[u32]) -> Vec<u32> {
    // a | (-b) = (a & (b - 1)) + 1 - b
    // 0 is not negative, so we can ignore it
    let sub1 = sub_two_slices(negative, &[1]);
    let or = bitand_two_slices(positive, &sub1);
    let add = add_two_slices(&or, &[1]);
    let sub2 = sub_two_slices(&add, negative);
    sub2
}

logic_op! {
    BitAnd, BitAndAssign, SmallInt, SmallIntType, bitand, bitand_assign;
    i, j, p, q, s, t;
    {
        let slice = unsafe { core::slice::from_raw_parts(p, s.unsigned_abs()) };
        match (i.cmp(&0), s.cmp(&0)) {
            (Ordering::Equal, _) => SmallInt(SmallIntType::Inline(0)),
            (Ordering::Greater, Ordering::Greater) => {
                let j = { heap_to_inline! { i128; slice } };
                SmallInt(SmallIntType::Inline(i & j))
            },
            (Ordering::Greater, Ordering::Less) => {
                let j = { heap_to_inline! { i128; slice } };
                SmallInt(SmallIntType::Inline(i & -j))
            },
            (Ordering::Less, Ordering::Greater) => {
                let mut res = <Box<[u32]>>::from(slice);
                let mut inline = i;
                for idx in 0..4 {
                    res[idx] &= inline as u32;
                    inline >>= 32;
                }
                return_heap_inner! { SmallInt, SmallIntType; res }
            },
            (Ordering::Less, Ordering::Less) => {
                let mut res = <Box<[u32]>>::from(slice);
                let j = { heap_to_inline! { i128; slice } };
                let lo = (i & j.wrapping_neg()).wrapping_neg();
                let mut lo_remaining = lo;
                for idx in 0..4 {
                    res[idx] = lo_remaining as u32;
                    lo_remaining >>= 32;
                }

                if lo == 0 && j != 0 {
                    let res2 = add_two_slices(&res, &[0, 0, 0, 0, 1]);
                    return_heap_neg! { SmallInt, SmallIntType; res2 }
                } else {
                    return_heap_inner_neg! { SmallInt, SmallIntType; res }
                }
            },
            (_, Ordering::Equal) => unreachable!("0 must be inline"),
        }
    },
    {
        let slice1 = unsafe { core::slice::from_raw_parts(p, s.unsigned_abs()) };
        let slice2 = unsafe { core::slice::from_raw_parts(q, t.unsigned_abs()) };
        match (s.cmp(&0), t.cmp(&0)) {
            (Ordering::Greater, Ordering::Greater) => {
                let min = std::cmp::min(slice1.len(), slice2.len());

                #[allow(unused_mut)]
                let mut res = { heap_heap_create_res_shortest! { bitand; slice1, slice2, min } };
                // TODO: shrink if needed, sometimes to Inline
                return_heap! { SmallInt, SmallIntType; res }
            },
            (Ordering::Greater, Ordering::Less) => {
                let res = bitand_two_slices_mixed_sign(slice1, slice2);
                return_heap! { SmallInt, SmallIntType; res }
            },
            (Ordering::Less, Ordering::Greater) => {
                let res = bitand_two_slices_mixed_sign(slice2, slice1);
                return_heap! { SmallInt, SmallIntType; res }
            },
            (Ordering::Less, Ordering::Less) => {
                // (-a) & (-b) = -(((a-1) | (b-1)) + 1)
                // 0 is not negative, so we can ignore it
                let sub1 = sub_two_slices(slice1, &[1]);
                let sub2 = sub_two_slices(slice2, &[1]);
                let tmp = bitor_two_slices(&sub1, &sub2);
                let res = add_two_slices(&tmp, &[1]);
                return_heap_neg! { SmallInt, SmallIntType; res }
            },
            (Ordering::Equal, _) | (_, Ordering::Equal) => unreachable!("0 must be inline"),
        }
    }
}

// logic_op! {
//     BitOr, BitOrAssign, SmallInt, SmallIntType, bitor, bitor_assign;
//     i, j, p, q, s, t;
//     {
//         let slice = unsafe { core::slice::from_raw_parts(p, s.unsigned_abs()) };
//         match (i.cmp(&0), s.cmp(&0)) {
//             (Ordering::Equal, _) => {
//                 let mut slice = ManuallyDrop::new(<Box<[_]>>::from(slice));
//                 SmallInt(SmallIntType::Heap((slice.as_mut_ptr(), s)))
//             },
//             (Ordering::Greater, Ordering::Greater) => {
//                 inline_heap_to_heap! { bitor_assign, SmallInt, SmallIntType; i, slice }
//             },
//             (Ordering::Greater, Ordering::Less) => {
//                 let mut res = <Box<[u32]>>::from(slice);
//                 let mut inline = i as u128;
//                 for idx in 0..4 {
//                     res[idx] = ((res[idx] as i32).wrapping_neg() | (inline as i32).wrapping_neg()).wrapping_neg() as u32;
//                     inline >>= 32;
//                 }
//                 return_heap_inner_neg! { SmallInt, SmallIntType; res }
//             },
//             (Ordering::Less, Ordering::Greater) => {
//                 let j = { heap_to_inline! { i128; slice } };
//                 SmallInt(SmallIntType::Inline(i | -j))
//             },
//             (Ordering::Less, Ordering::Less) => {
//                 let mut res = <Box<[u32]>>::from(slice);
//                 let j = { heap_to_inline! { i128; slice } };
//                 let lo = (i | j.wrapping_neg()).wrapping_neg();
//                 let mut lo_remaining = lo;
//                 for idx in 0..4 {
//                     res[idx] = lo_remaining as u32;
//                     lo_remaining >>= 32;
//                 }
//
//                 if lo == 0 && j != 0 {
//                     let res2 = add_two_slices(&res, &[0, 0, 0, 0, 1]);
//                     return_heap_neg! { SmallInt, SmallIntType; res2 }
//                 } else {
//                     return_heap_inner_neg! { SmallInt, SmallIntType; res }
//                 }
//             },
//             (_, Ordering::Equal) => unreachable!("0 must be inline"),
//         }
//     },
//     {
//         let slice1 = unsafe { core::slice::from_raw_parts(p, s.unsigned_abs()) };
//         let slice2 = unsafe { core::slice::from_raw_parts(q, t.unsigned_abs()) };
//         match (s.cmp(&0), t.cmp(&0)) {
//             (Ordering::Greater, Ordering::Greater) => {
//                 let min = std::cmp::min(slice1.len(), slice2.len());
//
//                 #[allow(unused_mut)]
//                 let mut res = { heap_heap_create_res_longest! { bitor; slice1, slice2, min } };
//
//                 return_heap! { SmallInt, SmallIntType; res }
//             },
//             (Ordering::Greater, Ordering::Less) => {
//                 let res = bitor_two_slices_mixed_sign(slice1, slice2);
//                 return_heap! { SmallInt, SmallIntType; res }
//             },
//             (Ordering::Less, Ordering::Greater) => {
//                 let res = bitor_two_slices_mixed_sign(slice2, slice1);
//                 return_heap! { SmallInt, SmallIntType; res }
//             },
//             (Ordering::Less, Ordering::Less) => {
//                 // (-a) | (-b) = -(((a-1) & (b-1)) + 1)
//                 // 0 is not negative, so we can ignore it
//                 let sub1 = sub_two_slices(slice1, &[1]);
//                 let sub2 = sub_two_slices(slice2, &[1]);
//                 let tmp = bitand_two_slices(&sub1, &sub2);
//                 let res = add_two_slices(&tmp, &[1]);
//                 return_heap_neg! { SmallInt, SmallIntType; res }
//             },
//             (Ordering::Equal, _) | (_, Ordering::Equal) => unreachable!("0 must be inline"),
//         }
//     }
// }
